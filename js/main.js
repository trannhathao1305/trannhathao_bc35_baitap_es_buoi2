let dataGlasses = [
    {
        id: "G1",
        src: "./img/g1.jpg",
        virtualImg: "./img/v1.png",
        brand: "Armani Exchange",
        name: "Bamboo wood",
        color: "Brown",
        price: 150,
        description:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
    },
    {
        id: "G2",
        src: "./img/g2.jpg",
        virtualImg: "./img/v2.png",
        brand: "Arnette",
        name: "American flag",
        color: "American flag",
        price: 150,
        description:
            "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
    },
    {
        id: "G3",
        src: "./img/g3.jpg",
        virtualImg: "./img/v3.png",
        brand: "Burberry",
        name: "Belt of Hippolyte",
        color: "Blue",
        price: 100,
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
    },
    {
        id: "G4",
        src: "./img/g4.jpg",
        virtualImg: "./img/v4.png",
        brand: "Coarch",
        name: "Cretan Bull",
        color: "Red",
        price: 100,
        description: "In assumenda earum eaque doloremque, tempore distinctio.",
    },
    {
        id: "G5",
        src: "./img/g5.jpg",
        virtualImg: "./img/v5.png",
        brand: "D&G",
        name: "JOYRIDE",
        color: "Gold",
        price: 180,
        description:
            "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
    },
    {
        id: "G6",
        src: "./img/g6.jpg",
        virtualImg: "./img/v6.png",
        brand: "Polo",
        name: "NATTY ICE",
        color: "Blue, White",
        price: 120,
        description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
    },
    {
        id: "G7",
        src: "./img/g7.jpg",
        virtualImg: "./img/v7.png",
        brand: "Ralph",
        name: "TORTOISE",
        color: "Black, Yellow",
        price: 120,
        description:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
    },
    {
        id: "G8",
        src: "./img/g8.jpg",
        virtualImg: "./img/v8.png",
        brand: "Polo",
        name: "NATTY ICE",
        color: "Red, Black",
        price: 120,
        description:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
    },
    {
        id: "G9",
        src: "./img/g9.jpg",
        virtualImg: "./img/v9.png",
        brand: "Coarch",
        name: "MIDNIGHT VIXEN REMIX",
        color: "Blue, Black",
        price: 120,
        description:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
    },
];

/** Defined Opject */
class glasses {
    //private properties
    #id;
    #src;
    #virtualImg;
    #brand;
    #name;
    #color;
    #price;
    #description;

    //initial constructor function
    constructor({ id, src, virtualImg, brand, name, color, price, description }) {
        this.#id = id;
        this.#src = src;
        this.#virtualImg = virtualImg;
        this.#brand = brand;
        this.#name = name;
        this.#color = color;
        this.#price = price;
        this.#description = description;
    }

    showGlasses() {
        return `<div id="${this.#id}" class="col-md-6 col-lg-4">
        <img src="${this.#src}" class="card-img-top" />
      </div>`;
    }

    mapToModel(glassesParam) {
        return {
            avt: `<img class="imgMapped" src=${glassesParam.virtualImg} />`,
            info: `<div class="gl__title">
                <span class="gl__name">${glassesParam.name}</span> - 
                <span class="gl__brand">${glassesParam.brand}</span> 
                (<span class="gl__color">${glassesParam.color}</span>)
                </div>
                <div class="gl__price">$${glassesParam.price}</div>
            <div class="gl__desc">${glassesParam.description}</div>
            `,
        };
    }
}

const main = () => {
    //Bind element
    const $ = document.querySelector.bind(document);

    //Defined Elements
    const glassesList = $("#vglassesList");
    const btnBf = $("#btnBf");
    const btnAt = $("#btnAt");
    let gls;

    //Generate glasses items DOM
    const glsList = dataGlasses.map((glassesItem) => {
        gls = new glasses(glassesItem);
        return gls.showGlasses();
    });

    //Map glasses list to glasses list Elememt
    glassesList.innerHTML = glsList.join().replaceAll(",", "");

    //Handle event map glass infos to model
    dataGlasses.map((glassesItem) => {
        const btnG = $(`#${glassesItem.id}`);
        const avatar = $("#avatar");
        const infoGlasses = $("#glassesInfo");
        btnG.addEventListener("click", () => {
            avatar.innerHTML = gls.mapToModel(glassesItem).avt;
            infoGlasses.innerHTML = gls.mapToModel(glassesItem).info;
            infoGlasses.style.display = "block";
        });
    });

    //Handle hide and show activate glass
    const removeGlasses = (isTrue) => {
        return isTrue
            ? ($(".imgMapped").style.display = " none")
            : ($(".imgMapped").style.display = "block");
    };

    btnBf.addEventListener("click", () => removeGlasses(true));
    btnAt.addEventListener("click", () => removeGlasses(false));
};

document.addEventListener("DOMContentLoaded", main);
